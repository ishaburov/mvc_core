import Vue from 'vue'
import Router from 'vue-router'
import Register from './views/Register.vue'
import Login from './views/Login.vue'
import NotFound from './views/NotFound.vue'
import Proffile from './views/Proffile.vue'

Vue.use(Router)
export default new Router({
  mode: "history",
  routes: [
    {
      path: '/',
      name: 'proffile',
      component: Proffile
    },
    {
      path: '/register',
      name: 'register',
      component: Register
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/404',
      name: '404',
      component: NotFound,
      meta: {auth: true}
    },
    {
      path: '*',
      redirect: '/404',
      meta: {auth: true}
    }
  ]
})
