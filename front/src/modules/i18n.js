import VueI18n from "vue-i18n";
import Vue from 'vue';

Vue.use(VueI18n);

import ru from '../lang/ru';
import en from '../lang/en';

const messages = Object.assign(ru, en);

let locale = localStorage.getItem('lang');
if (!locale){
  localStorage.setItem('lang', 'ru');
  locale = localStorage.getItem('lang');
}
const i18n = new VueI18n({
  locale, // set locale
  messages, // set locale messages
});

export default i18n;
