import axios from 'axios';
let baseUrl = process.env.VUE_APP_API_URL;


let makeRequest = async ({method, url, data = undefined, opt = undefined}) => {
    if (localStorage.getItem('token')){
      axios.defaults.headers.common['Authorization'] = localStorage.getItem('token');
    }
    axios.defaults.headers.post['Language'] = localStorage.getItem('lang');
    axios.defaults.headers.get['Language'] = localStorage.getItem('lang');
    url = baseUrl + url;
    try {
        let request = await axios({
            method,
            url,
            data,
            opt
        });

        return request;
    } catch (error) {
        console.log(error);
    }
};


export default {
    install(Vue) {
        Vue.prototype.$request = async (method, url, data, opt = null) => {
            let response = await makeRequest({
                method,
                url,
                data,
                opt
            });
            return response;
        };
        Vue.prototype.$request.get = async (url,  opt = null) => {
          let data = null;
            let response = await makeRequest({
                method: 'GET',
                url,
                data,
                opt,
            });
            return response;
        };
        Vue.prototype.$request.post = async (url, data, opt = null) => {
            let response = await makeRequest({
                method: 'POST',
                url,
                data,
                opt
            });
            return response;
        };
        Vue.prototype.$request.put = async (url, data) => {
            let response = await makeRequest({
                method: 'PUT',
                url,
                data,
            });
            return response;
        };
        Vue.prototype.$request.patch = async (url, data) => {
            let response = await makeRequest({
                method: 'PATCH',
                url,
                data,
            });
            return response;
        };
        Vue.prototype.$request.delete = async (url, data) => {
            let response = await makeRequest({
                method: 'DELETE',
                url,
                data,
            });
            return response;
        };
    }
};
