

export default {
  en: {
    titles:{
      'register' : 'registration',
      'login' : 'login'
    },
    form: {
      email: 'Your email',
      name:'Your name',
      about:'About me',
      password:'Your password',
      password_confirmation:'Password confirmation',
      avatar:'Your avatar'
    },
    proffile:{
      name: 'Name',
      about: 'About me',
      email: 'Email',
    },
  }
}
