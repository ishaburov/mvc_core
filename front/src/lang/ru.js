

export default {
  ru: {
    titles:{
      'register' : 'регистрация',
      'login' : 'вход'
    },
    form: {
      email: 'Ваша электронная почта',
      name:'Ваше имя',
      about:'Обо мне',
      password:'Ваш пароль',
      password_confirmation:'Подтверждение пароля',
      avatar:'Ваш аватар'
    },
    proffile:{
      name: 'Имя',
      about: 'Обо мне',
      email: 'Электронная почта',
    },
  }
}
