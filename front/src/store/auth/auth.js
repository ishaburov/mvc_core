

const auth = {

    state: {
        user: {},
    },
    actions: {
        setAuthUser(context,user){
           return context.commit('setAuthUser',user);
        },
    },

    mutations: {

        removeAuth(state,response){
            return this.auth = false
        },

        setAuth(state,data){
            return state = data
        },

        setAuthUser(state,data){
          return  state.user = data;
        },

    },

    getters: {
        getAuth(state) {
            return state;
        },
        getAuthUser(state){
            return  state.user
        }

    },

};

export default auth;
