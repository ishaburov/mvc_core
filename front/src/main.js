import Vue from 'vue';
import App from './App.vue';
import router from './router';
import vuetify from './modules/vuetify';
import lifeCycle from './api/lifecycle';
import VeeValidate, {Validator} from 'vee-validate';
import store from './store/index';
import WebFont from 'webfontloader';


Vue.use(VeeValidate);
Vue.use(lifeCycle);


import ru from 'vee-validate/dist/locale/ru';
import en from 'vee-validate/dist/locale/en';

Validator.localize('en', en);
Validator.localize('ru', ru);

import i18n from './modules/i18n';

WebFont.load({google: {families: ['Nunito']}});
Vue.config.productionTip = false;

new Vue({
  store,
  i18n,
  vuetify,
  router,
  render: h => h(App)
}).$mount('#app');
