<?php
/**
 * User: Vanya Shaburov  * Date: 16.02.2019  * Time: 2:53
 */

namespace App;


class Storage
{
    private $items = [];

    public function set($key, $object)
    {
        $this->items[$key] = $object;
    }

    public function get($key)
    {
        if (!isset($this->items[$key])) {
            $this->items[$key] = Application::call()->createComponent($key);
        }

        return $this->items[$key];
    }
}
