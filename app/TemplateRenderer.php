<?php
/**
 * User: Vanya Shaburov  * Date: 16.02.2019  * Time: 2:38
 */

namespace App;

use App\Interfaces\iRenderer;

class TemplateRenderer implements IRenderer
{
    public function render($template, $params = [])
    {
        ob_start();
        extract($params);
        $templatePath = Application::call()->config['templatesDir'].$template.".php";
        include $templatePath;

        return ob_get_clean();
    }
}
