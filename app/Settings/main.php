<?php
/**
 * User: Vanya Shaburov  * Date: 16.02.2019  * Time: 3:01
 */

return
    [
        'rootDir' => __DIR__."/../",
        'templatesDir' => __DIR__."/../views/",
        'appUrl' => 'http://api.test.ru/',
        'defaultController' => 'Base',
        'controllerNamespace' => "App\\Controllers",
        'queryLog' => true,
        'components' =>
            [
                'db' =>
                    [
                        'class' => \App\Services\DB::class,
                        'connection' => [
                            'driver' => 'mysql',
                            'host' => 'mysqldb',
                            'database' => 'test',
                            'username' => 'test',
                            'password' => '123456',
                            'charset' => 'utf8',
                            'collation' => 'utf8_unicode_ci',
                            'prefix' => '',
                        ],
                    ],
                'request' =>
                    [
                        'class' => \App\Request::class,
                    ],
                'templateRenderer' =>
                    [
                        'class' => \App\TemplateRenderer::class,
                    ],
                'lang' =>
                    [
                        'class' => \App\Services\Lang::class,
                    ],
            ],

    ];
