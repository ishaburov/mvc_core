<?php


namespace App\Requests;


use App\Application;
use App\Models\User;
use App\Request;

class UserRegisterRequest extends Request
{

    protected $email;
    protected $password;
    protected $password_confirmation;
    protected $name;
    protected $about;
    protected $errors;


    public function __construct()
    {
        parent::__construct();
        $this->email = $this->get('email');
        $this->password = $this->get('password');
        $this->password_confirmation = $this->get('password_confirmation');
        $this->name = $this->get('name');
        $this->about = $this->get('about');
    }

    public function validate()
    {

        $language = Application::$app->lang;

        $passwordErrors = $language->get()['errors']['password'];
        $emailErrors = $language->get()['errors']['email'];
        $errors = $language->get()['errors'];

        if (User::whereEmail($this->email)->exists()) {
            $this->errors[] = $emailErrors['exists'];
        }
        if (empty($this->password)) {
            $this->errors[] = $passwordErrors['empty'];
        }
        if (empty($this->name)) {
            $this->errors[] = $errors['name'];
        }
        if (empty($this->about)) {
            $this->errors[] = $errors['about'];
        }
        if (strlen($this->password) < 6) {
            $this->errors[] = $passwordErrors['len'];
        }
        if (!$this->passwordConfirmed()) {
            $this->errors[] = $passwordErrors['no_confirmed'];
        }
        if (empty($this->email)) {
            $this->errors[] = $emailErrors['empty'];
        }
        if (!$this->validEmail($this->email)) {
            $this->errors[] = $emailErrors['not_valid'];
        }

        return $this;
    }

    public function errors()
    {
        if (!empty($this->errors)) {
            return false;
        }

        return true;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    protected function validEmail($str)
    {
        return (!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $str)) ? false : true;
    }

    protected function passwordConfirmed()
    {
        if ($this->password !== $this->password_confirmation) {
            return false;
        }

        return true;
    }
}