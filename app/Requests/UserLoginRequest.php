<?php


namespace App\Requests;


use App\Application;
use App\Models\User;
use App\Request;

class UserLoginRequest extends Request
{

    protected $email;
    protected $password;
    protected $errors;

    public function __construct()
    {
        parent::__construct();

        $this->email = $this->get('email');
        $this->password = $this->get('password');
    }

    public function validate()
    {

        $language = Application::$app->lang;

        $passwordErrors = $language->get()['errors']['password'];
        $emailErrors = $language->get()['errors']['email'];

        if (!User::whereEmail($this->email)->exists()) {
            $this->errors[] = $emailErrors['not_exists'];
        }
        if (empty($this->password)) {
            $this->errors[] = $passwordErrors['empty'];
        }
        if (strlen($this->password) < 6) {
            $this->errors[] = $passwordErrors['len'];
        }
        if (empty($this->email)) {
            $this->errors[] = $emailErrors['empty'];
        }
        if (!$this->validEmail($this->email)) {
            $this->errors[] = $emailErrors['not_valid'];
        }

        return $this;
    }

    public function errors()
    {
        if (!empty($this->errors)) {
            return false;
        }

        return true;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    protected function validEmail($str)
    {
        return (!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $str)) ? false : true;
    }
}
