<?php
/**
 * User: Vanya Shaburov  * Date: 16.02.2019  * Time: 2:51
 */

namespace App;

use App\Services\Cors;
use App\Services\DB;
use App\Traits\TSingleton;


/**
 * Class App
 * @property Request $request;
 * @property DB $db;
 * @property interfaces\IRenderer $templateRenderer
 */
class Application
{
    use TSingleton;

    public $config;
    static $app;

    /** @var  Storage */
    private $components;
    protected $active = null;


    public static function call()
    {
        return static::getInstance();
    }

    public function run($config)
    {
        $this::$app = self::call();
        $this->config = $config;
        $this->components = new Storage();
        $this->db->load();

        $this->runController();

        if ($this->active) {
            return exit;
        }
    }


    private function runController()
    {
        Cors::get($this->request);
        $controllerName = $this->request->getControllerName() ?: $this->config['defaultController'];
        $action = $this->request->getActionName();
        $controllerClass = $this->config['controllerNamespace']."\\".ucfirst($controllerName)."Controller";
        try {
            if (!class_exists($controllerClass)) {
                return include "404.php";
            }

            $controller = new $controllerClass($this->templateRenderer);
            $controller->run($action);
            $this->active = true;
        }
        catch (\Exception $e) {
            $this->active = null;

            return false;
        }
    }

    /**
     * @param $name
     * @return object
     * @throws \ReflectionException
     */
    public function createComponent($name)
    {
        if (!isset($this->config['components'][$name])) {
            throw new \Exception("Компонент {$name} не описан!");
        }
        $params = $this->config['components'][$name];
        $class = $params['class'];
        if (!class_exists($class)) {
            throw new \Exception("Не определен класс компонента");
        }
        $reflection = new \ReflectionClass($class);
        unset($params['class']);

        return $reflection->newInstanceArgs($params);

    }

    /**
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        if (empty($name)) {
            return false;
        }

        return $this->components->get($name);
    }
}
