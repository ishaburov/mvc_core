<?php
/**
 * User: Vanya Shaburov  * Date: 16.02.2019  * Time: 2:55
 */

namespace App\Traits;


trait TSingleton
{
    private static $instance = null;

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    public static function getInstance()
    {
        if (is_null(static::$instance)) {
            static::$instance = new static();
        }

        return static::$instance;
    }
}
