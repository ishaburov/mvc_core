<?php


namespace App\Controllers;


use App\Application;
use app\Helpers\UserAvatarImage;
use App\Interfaces\iRenderer;
use App\Models\User;
use App\Requests\UserLoginRequest;
use App\Requests\UserRegisterRequest;
use Illuminate\Support\Facades\App;

class UserController extends Controller
{

    private $user;

    public function __construct()
    {
        $this->user = new User();
    }

    public function actionMe()
    {
      $token = $this->request
        ->headers->get('authorization');

      if (empty($token)) {
        return $this->json(null, ['Ошибка']);
      }

      $user = $this->user
        ->where('token', $token)
        ->first();

      if (!$user) {
        return $this->json(null, ['Ошибка']);
      }

      return $this->json($user);
    }

    public function actionLogin()
    {
        if(!$this->request->isPost()){
            return $this->json(null,['message' => 'Ошибка']);
        }
        $request = new UserLoginRequest();
        if (!$request->validate()->errors()){
            return $this->json(null, $request->getErrors());
        }

        $email = $request
            ->get('email');

        $password = $request
            ->get('password');

       $user = $this->user
         ->where('email', $email)
         ->first();

        $language = Application::$app->lang;
        $languageErrors = $language->get()['errors'];

       if (!$user){
           return $this->json(null,['message' => $languageErrors['user_not_found']]);
       }

       if ($user->password !== hash('sha256',$password)){
           return $this->json(null,['message' => $languageErrors['miss_password']]);
       }

       $user->token = str_random(50);
       $user->update();

       return $this->json($user);
    }

    public function actionRegister()
    {
        if(!$this->request->isPost()){
            return $this->json(null,['message' => 'Ошибка']);
        }
        $request = new UserRegisterRequest();
        if (!$request->validate()->errors()){
            return $this->json(null, $request->getErrors());
        }
        $file = $this->request->getFiles('file');
        $savedPath = "";
        if (!empty($file)){
         $image = new UserAvatarImage($file);
         $savedPath = $image->upload();
        }

        $password = $request->getParams('password');
        $data = array_merge($request->getParams(),[
          'avatar' => $savedPath,
          'password' => hash('sha256',$password)
        ]);

        $createdUser = $this->user
            ->create($data);

        $createdUser->token = str_random(50);
        $createdUser->update();

        return $this->json($createdUser);

    }
}
