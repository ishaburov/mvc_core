<?php

namespace App\Controllers;

use App\Application;
use App\Request;


/**
 * @property Request $request;
 */
abstract class Controller
{
    private $action;
    private $defaultAction = "index";
    private $layout = "main";
    private $useLayout = true;
    private $renderer;
    private $ajax;
    protected $request;


    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }


    public function run($action = null)
    {
        $this->action = $action ?: $this->defaultAction;
        $method = "action".ucfirst($this->action);
        $this->request = Application::call()->request;

        if (method_exists($this, $method)) {
            return $this->$method();
        }

        return include "../404.php";
    }


    public function render($template, $params = [])
    {
        if ($this->useLayout) {
            $content = $this->renderTemplate($template, $params);

            return $this->renderTemplate("layouts/{$this->layout}", ['content' => $content]);
        }

        return $this->renderTemplate($template, $params);
    }

    protected function renderTemplate($template, $params = [])
    {
        return $this->renderer->render($template, $params);
    }


    public function json($data = '', $errors = null)
    {
        $status = true;
        if (is_null($data)) {
            $status = false;
        }
        $this->jsonStatus($status);
        header('Content-Type: application/json');

        return print($this->jsonEncode($data, $errors));
    }

    public function jsonStatus($status = true)
    {
        $this->ajax['status'] = true;

        if (!$status) {
            $this->ajax['status'] = false;
        }

        return $this;
    }

    private function jsonEncode($data, $errors = null)
    {
        $this->ajax['data'] = $data;

        if (is_null($data)) {
            $this->ajax['data'] = null;
            $this->ajax['errors'] = $errors;
        }

        return json_encode($this->ajax, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
    }
}
