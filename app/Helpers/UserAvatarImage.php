<?php


namespace app\Helpers;


use App\Application;
use Spatie\Image\Image;

class UserAvatarImage extends Image
{
  protected $image;

  public function __construct($pathToImage)
  {
    parent::__construct($pathToImage);
    $this->image = $pathToImage;
  }

  public function upload($params = [])
  {
    $rootDir = Application::$app->config['rootDir'];
    if (!file_exists($rootDir.'public/images')) {
      mkdir($rootDir.'public/images');
    }
    $path = $rootDir.'public/images/';
    $imageName = str_random(15);

    $this->optimize()
      ->save($path.$imageName.".jpg");

    return 'images/'.$imageName.".jpg";
  }
}
