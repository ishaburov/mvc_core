<?php
/**
 * User: Vanya Shaburov  * Date: 16.02.2019  * Time: 2:37
 */

namespace App;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;

class Request extends SymfonyRequest
{
    private $controllerName;
    private $actionName;
    private $requestString;
    private $requestMethod;

    /**
     * Request constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->requestString = $_SERVER['REQUEST_URI'];
        $this->requestMethod = $_SERVER['REQUEST_METHOD'];
        $this->parseRequest();
        $this->initialize($_GET, $_REQUEST, [$_SERVER['REQUEST_URI'], $_SERVER['REQUEST_METHOD']], $_COOKIE, $_FILES, $_SERVER);
    }

    /** /product/card?id=2 */
    private function parseRequest()
    {
        $pattern = "#(?P<controller>\w+)[/]?(?P<action>\w+)?[/]?[?]?(?P<params>.*)#ui";
        if (preg_match_all($pattern, $this->requestString, $matches)) {
            $this->controllerName = $matches['controller'][0];
            $this->actionName = $matches['action'][0];
        }
    }


    /**
     * @return mixed
     */
    public function getControllerName()
    {
        return $this->controllerName;
    }

    /**
     * @return mixed
     */
    public function getActionName()
    {
        return $this->actionName;
    }

    /**
     * @return mixed
     */
    public function getParams($name = null)
    {
        if ($name) {
            return $this->request->get($name);
        }

        return $this->request->all();
    }

    /**
     * @param UploadedFile $name
     * @return array|mixed
     */
    public function getFiles($name = null)
    {
        if ($name) {
            return $this->files->get($name);
        }

        return $this->files->all();
    }

    public function getRequestMethod()
    {
        return $this->getMethod();
    }


    public function isGet()
    {

        return $this->isMethod("GET");
    }

    public function isPost()
    {
        return $this->isMethod("POST");
    }

    public function status()
    {
        return $this->server->get('REDIRECT_STATUS');
    }


}
