<?php
/**
 * User: Vanya Shaburov  * Date: 16.02.2019  * Time: 2:34
 */

namespace App\Interfaces;


interface iRenderer
{

	public function render($template, $params = []);

}
