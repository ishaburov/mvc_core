<?php

ini_set('error_reporting', 1);
error_reporting(E_ALL);

require_once "../vendor/autoload.php";
$config = include "../Settings/main.php";

$app = \App\Application::call()->run($config);
