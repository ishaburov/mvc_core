<?php


namespace App\Models;


use App\Application;
use App\Model;

class User extends Model
{
    protected $fillable = [
        'name',
        'about',
        'email',
        'password',
        'avatar'
    ];

    public function getAvatarAttribute($avatar)
    {
        return Application::$app->config['appUrl'].$avatar;
    }


    protected $hidden = ['password'];
}
