<?php

return [
  'errors' => [
      'email' => [
          'empty' => 'email is empty',
          'not_valid' => 'email not valid',
          'exists' => 'user with email exists',
          'not_exists' => 'user with email not exists'
      ],
      'password' => [
          'empty' => 'password is empty',
          'len' => 'password length bigger six',
          'no_confirmed' => 'password not confirmed',
      ],
      'about' => 'about not is empty',
      'name' => 'name not is empty',
      'user_not_found' => 'user not found',
      'miss_password' => 'password error try again'
  ]
];
