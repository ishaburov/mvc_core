<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 18.02.2019
 * Time: 22:49
 */

namespace App\Services;


use App\Application;
use Illuminate\Container\Container;
use Illuminate\Database\Capsule\Manager;
use Illuminate\Events\Dispatcher;

class DB extends Manager
{
    protected $config;


    public function __construct($connection, Container $container = null)
    {
        $this->config = $connection;
        parent::__construct($container);

    }

    public function load()
    {
        $this->addConnection($this->config);
        $this->setEventDispatcher(new Dispatcher(new Container()));
        $this->setAsGlobal();
        $this->bootEloquent();
        // \web\Asite::$app->db->getConnection()->getQueryLog());
        if (Application::$app->config['queryLog'] == true) {
            $this->getConnection()->enableQueryLog();
        }

    }

}
