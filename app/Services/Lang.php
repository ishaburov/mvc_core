<?php


namespace App\Services;


use App\Application;

class Lang
{
    protected $languages;
    protected $language;

    public function __construct()
    {
        $lang = Application::$app
            ->request
            ->headers
            ->get('Language', 'ru');

        $this->setDefault($lang);

        $this->languages['ru'] = include "../Lang/ru.php";
        $this->languages['en'] = include "../Lang/en.php";
    }

    public function setDefault($language = 'ru')
    {
        $this->language = $language;

        return $this;
    }

    public function get($lang = false)
    {
        if (!$lang) {
            $lang = $this->language;
        }

        return $this->languages[$lang];
    }
}
