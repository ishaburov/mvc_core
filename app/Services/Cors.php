<?php


namespace App\Services;


use App\Request;

class Cors
{
    /**
     * @param $request Request
     */
    public static function get($request)
    {
        if ($request->getMethod() === 'OPTIONS') {
            header('Access-Control-Allow-Origin: *');
            header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');
            header('Access-Control-Allow-Headers: Content-Type, Origin, Authorization, Language');
            header('Access-Control-Max-Age: 1728000');
            header('Content-Length: 0');
            header('Content-Type: application/json');
            die();
        }
        header('Access-Control-Allow-Headers: Content-Type, Origin, Authorization, Language');
        header('Access-Control-Allow-Origin: *');
        header('Content-Type: application/json');
    }
}
